export default {
	/*
	 ** Nuxt rendering mode
	 ** See https://nuxtjs.org/api/configuration-mode
	 */
	mode: 'universal',
	/*
	 ** Nuxt target
	 ** See https://nuxtjs.org/api/configuration-target
	 */
	target: 'static',
	/*
	 ** Headers of the page
	 ** See https://nuxtjs.org/api/configuration-head
	 */
	head: {
		title: "LifeLine",
		meta: [{
				charset: 'utf-8'
			},
			{
				name: 'viewport',
				content: 'width=device-width, initial-scale=1'
			},
			{
				hid: 'description',
				name: 'description',
				content: process.env.npm_package_description || ''
			}
		],
		link: [{
				rel: 'icon',
				type: 'image/x-icon',
				href: '/favicon.ico'
			},
			{
				href: "https://fonts.googleapis.com/css2?family=Raleway:wght@300;500&display=swap",
				rel: "stylesheet"
			}
		]
	},
	/*
	 ** Global CSS
	 */
	css: [
		"@/assets/global.css"
	],
	pageTransition: "page",
	/*
	 ** Plugins to load before mounting the App
	 ** https://nuxtjs.org/guide/plugins
	 */
	plugins: [
        "@/plugins/initMap.js",
        "@/plugins/swal.js"
	],
	/*
	 ** Auto import components
	 ** See https://nuxtjs.org/api/configuration-components
	 */
	components: true,
	/*
	 ** Nuxt.js dev-modules
	 */
	buildModules: [],
	/*
	 ** Nuxt.js modules
	 */
	modules: [
		// Doc: https://axios.nuxtjs.org/usage
		'nuxt-buefy',
		'@nuxtjs/axios',
	],
	/*
	 ** Axios module configuration
	 ** See https://axios.nuxtjs.org/options
	 */
	axios: {},             
	/*
	 ** Build configuration
	 ** See https://nuxtjs.org/api/configuration-build/
	 */
	build: {
		transpile: [/^vue2-google-maps($|\/)/, /^vue-sweetalert2($|\/)/] 
	},

	env: {
        apiURL: process.NODE_ENV !== "production" ?
            "https://e1325e6def4c.ngrok.io/api/v1":
            "PROD_SERVER_URL.GG",
        demoMode: process.NODE_ENV !== "production"
	}
}
