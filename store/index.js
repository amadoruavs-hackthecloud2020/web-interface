export const state = () => ({
    isLoggedIn: false,
    accessToken: null,
    refreshToken: null,
    username: null
});

export const mutations = {
    LOG_IN: (state, { username, accessToken, refreshToken }) => {
        state.isLoggedIn = true;
        state.username = username;
        state.accessToken = accessToken;
        state.refreshToken = refreshToken;
    },

    LOG_OUT: state => {
        state.isLoggedIn = false;
        state.username = null;
        state.accessToken = null;
        state.refreshToken = null;
    }
}